//
//  GameScene.swift
//  miney
//
//  Created by Geddy on 11/6/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import SpriteKit
import GameplayKit

var player = SKSpriteNode()
var mine = SKSpriteNode()
var goal = SKSpriteNode()

var lblMain = SKLabelNode()
var lblScore = SKLabelNode()

var touchedNode = SKNode()

var offBlackColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)
var offWhiteColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1.0)
var greenCustomColor = UIColor(red: (0/255), green: (128/255), blue: (64/255), alpha: 1.0)

var playerSize = CGSize(width: 60, height: 60)
var goalSize = CGSize(width: 45, height: 45)
var mineSize = CGSize(width: 20, height: 20)

var goalPosition0 = CGPoint(x: -275, y: -600)
var goalPosition1 = CGPoint(x: 275, y: 600)

var touchLocation = CGPoint()

var mineQuantity = 4

var goalLocation = 0
var hasReachedGoal = false

var score = 0

struct physicsCategory {
    static let player: UInt32 = 1
    static let mine: UInt32 = 2
    static let goal: UInt32 = 3
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    override func didMove(to view: SKView) {
        self.backgroundColor = greenCustomColor
        
        physicsWorld.contactDelegate = self
        resetVariables()
        
        
        spawnGoal()
        spawnPlayer()
        
        randomlyGenerateMines()
        
    }
    
    func resetVariables() {
        spawnLblMain()
        spawnLblScore()
        mineQuantity = 3
        
        goalLocation = 0
        hasReachedGoal = false
        
        score = 0
        
        lblMain.text = "Drag and Avoid Mines"
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            touchLocation = touch.location(in: self)
            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            touchLocation = touch.location(in: self)
            movePlayer()
        }
    }
    
    func movePlayer() {
        touchedNode = atPoint(touchLocation)
        
        player.position = touchLocation
        
    }
    
    func spawnPlayer() {
        player = SKSpriteNode(imageNamed: "player")
        player.size = playerSize
        
        player.position = goalPosition1
        
        player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        player.physicsBody?.affectedByGravity = false
        player.physicsBody?.categoryBitMask = physicsCategory.player
        player.physicsBody?.contactTestBitMask = physicsCategory.goal
        player.physicsBody?.isDynamic = false
        player.physicsBody?.allowsRotation = false
        
        player.name = "playerName"
        
        self.addChild(player)
        
    }
    
    func spawnMine() {
        
        let randomX = Int(arc4random_uniform(UInt32(210 - -255 + 1))) + -255
        let randomY = Int(arc4random_uniform(UInt32(570 - -550 + 1))) + -550
        
        mine = SKSpriteNode(imageNamed: "mine")
        mine.size = mineSize
        
        mine.position = CGPoint(x: randomX, y: randomY)
        
        mine.physicsBody = SKPhysicsBody(rectangleOf: mine.size)
        mine.physicsBody?.affectedByGravity = false
        mine.physicsBody?.categoryBitMask = physicsCategory.mine
        mine.physicsBody?.contactTestBitMask = physicsCategory.player
        mine.physicsBody?.isDynamic = true
        mine.physicsBody?.allowsRotation = false
        
        mine.name = "mineName"
        
        self.addChild(mine)
        
    }
    
    func spawnGoal() {
        goal = SKSpriteNode(imageNamed: "goal")
        goal.size = goalSize
        
        goal.position = goalPosition0
        
        goal.physicsBody = SKPhysicsBody(rectangleOf: goal.size)
        goal.physicsBody?.affectedByGravity = false
        goal.physicsBody?.categoryBitMask = physicsCategory.goal
        goal.physicsBody?.contactTestBitMask = physicsCategory.player
        goal.physicsBody?.isDynamic = true
        goal.physicsBody?.allowsRotation = false
        
        goal.name = "goalName"
        
        self.addChild(goal)
        
    }
    
    func spawnLblMain() {
        lblMain = SKLabelNode(fontNamed: "Futura")
        lblMain.fontColor = offWhiteColor
        lblMain.fontSize = 50
        lblMain.alpha = 0.5
        lblMain.zPosition = -1
        lblMain.position = CGPoint(x: self.frame.midX, y: self.frame.midY + 170)
        lblMain.text = "Drag and Avoid Mines"
        
        self.addChild(lblMain)
    }
    
    func spawnLblScore() {
        lblScore = SKLabelNode(fontNamed: "Futura")
        lblScore.fontColor = offWhiteColor
        lblScore.fontSize = 80
        lblScore.alpha = 0.5
        lblScore.zPosition = -1
        lblScore.position = CGPoint(x: self.frame.midX, y: self.frame.midY - 350)
        lblScore.text = "\(score)"
        
        self.addChild(lblScore)
    }
    
    func randomlyGenerateMines() {
        for _ in 1...mineQuantity {
            spawnMine()
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody = contact.bodyA
        var secondBody = contact.bodyB
        
        if ((firstBody.categoryBitMask == physicsCategory.player) && (secondBody.categoryBitMask == physicsCategory.mine) || (firstBody.categoryBitMask == physicsCategory.mine) && (secondBody.categoryBitMask == physicsCategory.player)) {
            playerMineContact(contactA: firstBody.node as! SKSpriteNode, contactB: secondBody.node as! SKSpriteNode)
        }
        
        
        if ((firstBody.categoryBitMask == physicsCategory.player) && (secondBody.categoryBitMask == physicsCategory.goal) || (firstBody.categoryBitMask == physicsCategory.goal) && (secondBody.categoryBitMask == physicsCategory.player)) {
            playerGoalContact(contactA: firstBody.node as! SKSpriteNode, contactB: secondBody.node as! SKSpriteNode)
        }
    }
    
    func playerMineContact(contactA: SKSpriteNode, contactB: SKSpriteNode) {
        
    }
    
    func playerGoalContact(contactA: SKSpriteNode, contactB: SKSpriteNode) {
        hasReachedGoal = true
        
        if goalLocation == 0 && hasReachedGoal == true {
            goalLocation = 1
            hasReachedGoal = false
        }
        
        if goalLocation == 1 && hasReachedGoal == true {
            goalLocation = 0
            hasReachedGoal = false
        }
        
        mineQuantity += 1
        score += 1
        updateScore()
        destroyAllMines()
        randomlyGenerateMines()
    }
    
    func updateScore() {
        lblScore.text = "\(score)"
    }
    
    func destroyAllMines() {
        
        self.enumerateChildNodes(withName: "mineName", using: { node, stop in
            if let sprite = node as? SKSpriteNode {
                sprite.removeFromParent()
            }
            
        })
    }
    
    func goalPositionLogic() {
        if goalLocation == 0 {
            goal.position = goalPosition0
        }
        
        if goalLocation == 1 {
            goal.position = goalPosition1
        }
    }
    
    func gameOverLogic() {
        destroyAllMines()
        player.removeFromParent()
        goal.removeFromParent()
        lblMain.alpha = 1.0
        lblMain.text = "Game Over"
        
        lblScore.alpha = 1.0
        
        resetGame()
    }
    
    func resetGame() {
        let wait = SKAction.wait(forDuration: 3.0)
        let theGameScene = GameScene(size: self.size)
        
        theGameScene.scaleMode = .aspectFill
        
        let theTransition = SKTransition.fade(with: greenCustomColor, duration: 1.0)
        
        let changeScene = SKAction.run {
            self.scene?.view?.presentScene(theGameScene, transition: theTransition)
        }
        
        let sequence = SKAction.sequence([wait, changeScene])
        self.run(SKAction.repeat(sequence, count: 1))
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        goalPositionLogic()
    }
}
